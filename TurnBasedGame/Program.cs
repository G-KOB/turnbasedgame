﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnBasedGame.Game;

namespace TurnBasedGame
{
    class Program
    {
        private static HeroRepository heroRepository = new HeroRepository();
        private static PlayerRepository playerRepository = new PlayerRepository();
        private static TurnElementRepository turnElementRepository = new TurnElementRepository();
        static bool allEnemiesAreDead = false; //this variable can be called everywhere in the scope of program, not just main
        static bool playerEndedGame = false;

        static void Main(string[] args)
        {
            Initialize(); //this function initializes all variables needed for the game
            Renderer.ShowIntroMessage();
            TurnSystem.StartTurn(turnElementRepository);

            ConsoleKeyInfo pressedKey = Renderer.ReadUserInput();
            do
            {
                var playerHeroes = heroRepository.GetHeroesByPlayer(Player.PlayerName);
                var playerHeroNames = playerHeroes.Select(ph => ph.Name).ToList();
                var enemyHeroes = heroRepository.GetHeroesByPlayer(Player.EnemyName);
                var enemyHeroNames = enemyHeroes.Select(eh => eh.Name).ToList();

                Renderer.ShowPlayerHeroInfo(playerHeroes);
                Renderer.ShowEnemyHeroInfo(enemyHeroes);

                var playableElements = TurnSystem.GetUsableTurnElements(turnElementRepository);
                if (playableElements.Count() == 0)
                {
                    TurnSystem.EndTurn(turnElementRepository, playerRepository);
                    continue;
                }

                if (turnElementRepository.CurrentPlayer.Type != PlayerType.Human)
                {
                    ComputerPlayTurn();
                    continue;
                }

                pressedKey = Renderer.ReadUserInput();
                switch(pressedKey.Key)
                {
                    case ConsoleKey.A:
                        string heroName = Renderer.CreateSelectMenu("Hero to attack with: ", playerHeroNames);
                        Hero hero = heroRepository.GetHero(Player.PlayerName, heroName);
                        string enemyHeroName = Renderer.CreateSelectMenu("Enemy to attack: ", enemyHeroNames);
                        Hero enemyHero = heroRepository.GetHero(Player.EnemyName,enemyHeroName);
                        HeroSystem.Attack(hero, enemyHero, heroRepository);
                    break;

                    case ConsoleKey.S:
                        heroName = Renderer.CreateSelectMenu("Hero to use spell with: ", playerHeroNames);
                        hero = heroRepository.GetHero(Player.PlayerName, heroName);
                        HeroSystem.UseSpell(hero, playerRepository, heroRepository);
                    break;

                    case ConsoleKey.Q:
                        playerEndedGame = true;
                        break;

                    default:
                        Renderer.WriteTemporaryLine("Unknown command");
                    break;
                }

                if (heroRepository.GetHeroCount(Player.EnemyName) == 0)
                {
                    OnAllEnemiesAreDead();
                }

            } while (!playerEndedGame && !allEnemiesAreDead); //repeat logic until user presses 'Q' or all enemies are dead;
        }

        static void ComputerPlayTurn()
        {
            TurnSystem.EndTurn(turnElementRepository, playerRepository);
        }

        static void OnAllEnemiesAreDead()
        {
            allEnemiesAreDead = true;
            Console.WriteLine("Enemies were killed!");
            Console.WriteLine("You won!");
            Console.Write("Press any key to quit...");
            Console.ReadKey();
        }

        static void Initialize()
        {
            playerRepository.Initialize();
            turnElementRepository.Initialize(playerRepository.GetPlayer(Player.PlayerName));
            heroRepository.InitializePlayerHeroes(Player.PlayerName, OnHeroAdded);
            heroRepository.InitializeEnemyHeroes(Player.EnemyName, OnHeroAdded);
            
        }

        static void OnHeroAdded(Hero hero)
        {
            TurnSystem.OnTurnElementInitialized(hero, turnElementRepository);
        }
    }
}
