﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame.Game
{
    public class TurnElementRepository
    {
        public List<ITurnElement> TurnElements { get; set; } = new List<ITurnElement>();
        public Player CurrentPlayer { get; set; }


        public IEnumerable<ITurnElement> GetUsableTurnElements(string owner)
        {
            return TurnElements.Where(te => te.Owner == owner && te.RemainingActions > 0);
        }

        public void RemoveTurnElement(ITurnElement turnElement)
        {
            TurnElements.Remove(turnElement);
        }

        public void AddTurnElement(ITurnElement turnElement)
        {
            if (!TurnElements.Contains(turnElement))
            {
                TurnElements.Add(turnElement);
            }
        }

        public void Initialize(Player playerToStart)
        {
            CurrentPlayer = playerToStart;
        }
    }
}
