﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame.Game
{
    public class PlayerRepository
    {
        public List<Player> Players { get; private set; } = new List<Player>();

        public Player GetPlayer(string name)
        {
            return Players.FirstOrDefault(p => p.Name == name);
        }

        public void Initialize()
        {
            Player player = new Player();
            player.Name = Player.PlayerName;
            player.Type = PlayerType.Human;

            Player enemy = new Player();
            enemy.Name = Player.EnemyName;
            enemy.Type = PlayerType.Computer;

            Players.Add(player);
            Players.Add(enemy);
        }
    }
}
