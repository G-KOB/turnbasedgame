﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame.Game
{
    public static class TurnSystem
    {
        public static void ResetTurn(TurnElementRepository turnElementRepository, string owner)
        {
            foreach (var turnElement in turnElementRepository.TurnElements.Where(te => te.Owner == owner))
            {
                turnElement.RemainingActions = turnElement.ActionsPerTurn;
            }
        }

        public static bool UseAction(ITurnElement turnElement, int actionCost)
        {
            if (CanUseAction(turnElement, actionCost))
            {
                turnElement.RemainingActions -= actionCost;
            }

            return false;
        }

        public static IEnumerable<ITurnElement> GetUsableTurnElements(TurnElementRepository turnElementRepository)
        {
            return turnElementRepository.GetUsableTurnElements(turnElementRepository.CurrentPlayer.Name);
        }

        public static void StartTurn(TurnElementRepository turnElementRepository)
        {
            ResetTurn(turnElementRepository, turnElementRepository.CurrentPlayer.Name);
        }

        public static void EndTurn(TurnElementRepository turnElementRepository, PlayerRepository playerRepository)
        {
            int currentPlayerIndex = playerRepository.Players.IndexOf(turnElementRepository.CurrentPlayer);
            int nextPlayerIndex = (currentPlayerIndex + 1) % playerRepository.Players.Count;
            turnElementRepository.CurrentPlayer = playerRepository.Players[nextPlayerIndex];
            StartTurn(turnElementRepository);
        }

        public static void OnTurnElementInitialized(ITurnElement turnElement, TurnElementRepository turnElementRepository)
        {
            turnElementRepository.AddTurnElement(turnElement);
        }

        private static bool CanUseAction(ITurnElement turnElement, int actionCost)
        {
            return turnElement.RemainingActions >= actionCost;
        }
    }
}
