﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame.Game
{
    public enum PlayerType
    {
        Computer,
        Human,
        None
    }

    public class Player
    {
        public const string EnemyName = "enemy";
        public const string PlayerName = "player";

        public static Player Undefined => new Player{ Name = "Undefined", Type = PlayerType.None };

        public string Name { get; set; }
        public PlayerType Type { get; set; }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Type.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Player other = obj as Player;
            if (other == null)
            {
                return false;
            }

            return this.GetHashCode() == other.GetHashCode();
        }
    }
}
