﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnBasedGame.Game;
using TurnBasedGame.Spells;

namespace TurnBasedGame
{
    /// <summary>
    /// container for hero data and its manipulation
    /// </summary>
    public class HeroRepository
    {
        //private set means that only functions in this scope can change the Property
        public List<Hero> Heroes { get; private set; } = new List<Hero>();

        private List<string> initializedPlayers = new List<string>();

        public IEnumerable<Hero> GetHeroesByPlayer(string playerName)
        {
            return Heroes.Where(h => h.Owner == playerName);
        }

        public IEnumerable<Hero> GetHeroesByHero(string heroName)
        {
            return Heroes.Where(h => h.Name == heroName);
        }

        public Hero GetHero(string playerName, string name)
        {
            return Heroes.FirstOrDefault(h => h.Owner == playerName && h.Name == name);
        }

        public int GetHeroCount(string playerName)
        {
            return GetHeroesByPlayer(playerName).Count();
        }

        public string[] GetHeroInfos(string playerName)
        {
            string[] heroInfos = new string[Heroes.Count];

            int i = 0;

            var heroes = GetHeroesByPlayer(playerName);

            //this is very similar loop to for, except it works better with non-indexed collections like Dictionary
            foreach(Hero hero in heroes)
            {
                heroInfos[i] = $"Hero: {hero.Name} | Health: {hero.Health} | Armor: {hero.Armor} | Magic Resist: {hero.MagicResist}";
                i++;
            }

            return heroInfos;
        }

        public void InitializePlayerHeroes(string playerName, Action<Hero> onHeroAdded)
        {
            if (initializedPlayers.Contains(playerName))
            {
                return;
            }

            //Heroes

            Hero garen = new Hero();
            garen.Name = "Garen";
            garen.AttackDamage = 15;
            garen.SpellPower = 0;
            garen.Health = 120;
            garen.Armor = 10;
            garen.MagicResist = 30;
            garen.Owner = playerName;

            Hero ryze = new Hero();
            ryze.Name = "Ryze";
            ryze.AttackDamage = 10;
            ryze.SpellPower = 20;
            ryze.Health = 100;
            ryze.Armor = 5;
            ryze.MagicResist = 10;
            ryze.Owner = playerName;
            ryze.Spells.Add(new Spell("Super Blast", false, false, true, RyzeSpells.SuperBlast));
            ryze.Spells.Add(new Spell("Magical Pierce", false, false, true, RyzeSpells.MagicalPierce));

            Hero ashe = new Hero();
            ashe.Name = "Ashe";
            ashe.AttackDamage = 20;
            ashe.SpellPower = 5;
            ashe.Health = 50;
            ashe.Armor = 5;
            ashe.MagicResist = 5;
            ashe.Owner = playerName;

            Hero janna = new Hero();
            janna.Name = "Janna";
            janna.AttackDamage = 5;
            janna.SpellPower = 10;
            janna.Health = 50;
            janna.Armor = 5;
            janna.MagicResist = 5;
            janna.Owner = playerName;
            janna.Spells.Add(new Spell("Power of the wind", true, false, false, JannaSpells.PowerOfTheWind));

            AddHero(garen, onHeroAdded);
            AddHero(ryze, onHeroAdded);
            AddHero(ashe, onHeroAdded);
            AddHero(janna, onHeroAdded);

            initializedPlayers.Add(playerName);
        }

        public void InitializeEnemyHeroes(string playerName, Action<Hero> onHeroAdded)
        {
            if (initializedPlayers.Contains(playerName))
            {
                return;
            }

            //Enemy heroes

            Hero darius = new Hero();
            darius.Name = "Darius";
            darius.AttackDamage = 15;
            darius.SpellPower = 0;
            darius.Health = 120;
            darius.Armor = 30;
            darius.MagicResist = 10;
            darius.Owner = playerName;

            Hero syndra = new Hero();
            syndra.Name = "Syndra";
            syndra.AttackDamage = 5;
            syndra.SpellPower = 20;
            syndra.Health = 100;
            syndra.Armor = 5;
            syndra.MagicResist = 10;
            syndra.Owner = playerName;

            Hero kaisa = new Hero();
            kaisa.Name = "Kaisa";
            kaisa.AttackDamage = 20;
            kaisa.SpellPower = 5;
            kaisa.Health = 10;
            kaisa.Armor = 5;
            kaisa.MagicResist = 5;
            kaisa.Owner = playerName;

            Hero pyke = new Hero();
            pyke.Name = "Pyke";
            pyke.AttackDamage = 10;
            pyke.SpellPower = 10;
            pyke.Health = 70;
            pyke.Armor = 5;
            pyke.MagicResist = 5;
            pyke.Owner = playerName;

            AddHero(darius, onHeroAdded);
            AddHero(syndra, onHeroAdded);
            AddHero(kaisa, onHeroAdded);
            AddHero(pyke, onHeroAdded);

            initializedPlayers.Add(playerName);
        }

        private void AddHero(Hero hero, Action<Hero> onHeroAdded)
        {
            if (!Heroes.Contains(hero))
            {
                Heroes.Add(hero);
                onHeroAdded(hero);
            }
        }
    }
}
