﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnBasedGame.Game;

namespace TurnBasedGame
{
    public static class HeroSystem
    {
        public static void Attack(Hero hero, Hero enemyHero, HeroRepository enemyHeroRepository)
        {
            Renderer.StartTemporaryBlock();

            Renderer.WriteTemporaryLine($"{hero.Name} attacked the enemy {enemyHero.Name}");
            DamageEnemyHero(enemyHero, hero.AttackDamage, DamageType.Physical);

            if (enemyHero.Health < 0)
            {
                enemyHeroRepository.Heroes.Remove(enemyHero);
                Renderer.WriteToTemporaryBlock($"Enemy {enemyHero.Name} was killed!");
            }

            Renderer.EndTemporaryBlock();
        }

        public static void UseSpell(Hero caster, PlayerRepository playerRepository, HeroRepository heroRepository)
        {
            string chosenSpellName = Renderer.CreateSelectMenu("Choose Spell: ", caster.Spells.Select(s => s.Name).ToList()); //Select loops through collection and creates new object from each item

            if (string.IsNullOrEmpty(chosenSpellName))
            {
                Renderer.WriteTemporaryLine("Hero has no spells to use");
                return;
            }

            Spell chosenSpell = caster.Spells.FirstOrDefault(s => s.Name == chosenSpellName); //First or default loops through collection and returns first element that meets condition

            List<Hero> targetsToChoose = new List<Hero>();

            targetsToChoose.AddRange(heroRepository.Heroes);

            if (!chosenSpell.CanCastOnAllies)
            {
                targetsToChoose.RemoveAll(h => h.Owner == caster.Owner);
            }

            if (!chosenSpell.CanCastOnSelf)
            {
                targetsToChoose.Remove(caster);
            }

            if (!chosenSpell.CanCastOnEnemies)
            {
                targetsToChoose.RemoveAll(h => h.Owner != caster.Owner);
            }

            string targetName = Renderer.CreateSelectMenu("Use spell on: ", targetsToChoose.Select(h => h.Name).ToList());
            var targetHeroes = heroRepository.GetHeroesByHero(targetName).ToList();
            //if more heroes are returned that means that more players owns same hero, so caster needs to select which player he means
            if (targetHeroes.Count > 1)
            {
                var targetPlayers = new List<string>();

                targetPlayers.AddRange(targetHeroes.Select(th => th.Owner));

                if (!chosenSpell.CanCastOnAllies)
                {
                    targetPlayers.RemoveAll(tp => tp == caster.Owner);
                }

                string targetPlayerName = Renderer.CreateSelectMenu("Select player that owns the hero: ", targetPlayers);
                targetHeroes.RemoveAll(th => th.Owner != targetPlayerName);
            }

            Hero targetHero = targetHeroes.FirstOrDefault();

            Renderer.StartTemporaryBlock();

            chosenSpell.Cast(caster, targetHero);

            if (targetHero.Health < 0)
            {
                heroRepository.Heroes.Remove(targetHero);
                Renderer.WriteToTemporaryBlock($"Hero {targetHero.Name} was killed!");
            }

            Renderer.EndTemporaryBlock();
        }

        public static void DamageEnemyHero(Hero enemyHero, int damage, DamageType damageType)
        {
            int mitigatedDamage = GetMitigatedDamage(damage, enemyHero, damageType);
            Renderer.WriteToTemporaryBlock($"Enemy {enemyHero.Name} recieved {mitigatedDamage} damage");
            enemyHero.Health -= mitigatedDamage;
        }

        public static void ShredMagicResist(Hero enemyHero, int amountToShred)
        {
            Renderer.WriteToTemporaryBlock($"Enemy {enemyHero.Name} lost {amountToShred} magic resist");
            enemyHero.MagicResist -= amountToShred;
        }

        public static void IncreasePower(Hero hero, int powerToIncrease)
        {
            Renderer.WriteToTemporaryBlock($"Hero {hero.Name} got spell power increased by {powerToIncrease}");
            hero.SpellPower += powerToIncrease;
        }

        private static int GetMitigatedDamage(int damage, Hero hero, DamageType damageType)
        {
            float mitigation = 0;
            switch (damageType)
            {
                case DamageType.Physical:
                    mitigation = damage * (hero.Armor / 100);
                    break;
                case DamageType.Magic:
                    mitigation = damage * (hero.MagicResist / 100);
                    break;
            }

            float mitigatedDamage = damage - mitigation;
            return (int)mitigatedDamage;
        }
    }
}
