﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame
{
    public interface IOwnable
    {
        string Owner { get; set; }
    }

    public interface ITurnElement : IOwnable
    {
        int ActionsPerTurn { get; }
        int RemainingActions { get; set; }
    }

    public interface IDamagable
    {
        int Health { get; set; }
        float Armor { get; set; }
        float MagicResist { get; set; }
    }

    public interface IDamaging
    {
        int AttackDamage { get; set; }
        int SpellPower { get; set; }
        List<Spell> Spells { get; set; }
    }

    public class Spell
    {
        public bool CanCastOnAllies { get; private set; }
        public bool CanCastOnSelf { get; private set; }
        public bool CanCastOnEnemies { get; private set; }
        public string Name { get; private set; }
        private Action<string, Hero, Hero> onCastSpell = null;

        //this is called constructor, this logic specify what should be done to construct an object
        public Spell(string name, bool canCastOnAllies, bool canCastOnSelf, bool canCastOnEnemies, Action<string, Hero, Hero> onCastSpell)
        {
            CanCastOnAllies = canCastOnAllies;
            CanCastOnEnemies = canCastOnEnemies;
            CanCastOnSelf = canCastOnSelf;
            Name = name;
            this.onCastSpell = onCastSpell; //"this" is specification of this instance, since both have same name we have to specify which variable is assigned to what
        }

        public void Cast(Hero source, Hero target)
        {
            onCastSpell(Name, source, target);
        }
    }

    public class Hero : IDamagable, IDamaging, ITurnElement
    {
        //this is one way of initializing a struct Property
        public static Hero UnknownHero => new Hero
        {
            Name = "Unknown",
            AttackDamage = 0,
            SpellPower = 0
        };

        //This is called a Property, for now treat it like a variable
        public string Owner { get; set; }
        public int ActionsPerTurn { get; set; }
        public int RemainingActions { get; set; }
        public string Name { get; set; }
        public int AttackDamage { get; set; }
        public int SpellPower { get; set; }
        public int Health { get; set; }
        public float Armor { get; set; }
        public float MagicResist { get; set; }
        public List<Spell> Spells { get; set; } = new List<Spell>(); //List is also an Collection like Array or Dictionary

        public override int GetHashCode()
        {
            return this.Owner.GetHashCode() ^ this.Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Hero other = obj as Hero;
            if (other == null)
            {
                return false;
            }

            return other.GetHashCode() == this.GetHashCode();
        }
    }

    public enum DamageType
    {
        Physical,
        Magic,
        True
    }
}
