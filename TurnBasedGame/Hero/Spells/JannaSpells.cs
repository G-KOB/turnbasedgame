﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame.Spells
{
    public static class JannaSpells
    {
        public static void PowerOfTheWind(string spellName, Hero source, Hero target)
        {
            int powerIncrease = (int)(source.SpellPower * 1);
            Renderer.WriteToTemporaryBlock($"{source.Name} used {spellName} on {target.Name}");
            HeroSystem.IncreasePower(target, powerIncrease);
        }
    }
}
