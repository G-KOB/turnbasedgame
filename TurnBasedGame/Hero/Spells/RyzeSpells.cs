﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame.Spells
{
    public static class RyzeSpells
    {
        public static void SuperBlast(string spellName, Hero source, Hero target)
        {
            int damage = (int)(source.SpellPower * 1.1f);

            Renderer.WriteToTemporaryBlock($"{source.Name} used {spellName} on the enemy {target.Name}");
            HeroSystem.DamageEnemyHero(target, damage, DamageType.Magic);
        }

        public static void MagicalPierce(string spellName, Hero source, Hero target)
        {
            int magicResistShred = (int)(source.SpellPower * 0.2f);

            Renderer.WriteToTemporaryBlock($"{source.Name} used {spellName} on the enemy {target.Name}");
            HeroSystem.ShredMagicResist(target, magicResistShred);
        }
    }
}
