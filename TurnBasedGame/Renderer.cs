﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedGame
{
    /// <summary>
    /// This is just a utility container for rendering text on the screen
    /// </summary>
    public static class Renderer
    {
        const string NameTitle = "Name";
        const string AttackDamageTitle = "Attack Damage";
        const string SpellPowerTitle = "Spell Power";
        const string HealthTitle = "Health";
        const string ArmorTitle = "Armor";
        const string MagicResistTitle = "Magic Resist";

        static int headerEndPosition;
        static int enemyInfoEndPosition;
        static int playerInfoEndPosition;
        static int longestEnemyInfo;
        static int temporaryLinePosition = -1;
        static bool temporaryBlockStarted = false;

        public static void ShowPlayerHeroInfo(IEnumerable<Hero> heroes)
        {
            int defaultCursorPosition = Console.CursorTop;

            ShowHeroInfo(" Player Heroes ", headerEndPosition, playerInfoEndPosition, heroes.ToArray());

            playerInfoEndPosition = Console.CursorTop;

            if (defaultCursorPosition > Console.CursorTop)
            {
                Console.CursorTop = defaultCursorPosition;
            }
        }

        public static void ShowEnemyHeroInfo(IEnumerable<Hero> heroes)
        {
            int defaultCursorPosition = Console.CursorTop;

            ShowHeroInfo(" Enemies ", playerInfoEndPosition, enemyInfoEndPosition, heroes.ToArray());

            enemyInfoEndPosition = Console.CursorTop - 1;

            if (defaultCursorPosition > Console.CursorTop)
            {
                Console.CursorTop = defaultCursorPosition;
            }
        }

        public static void ShowHeroInfo(string title, int startPosition, int endPosition, Hero[] heroes) //public means that Program can see this even if its not in its scope
        {
            //clear info
            for (int i = endPosition; i >= startPosition; i--)
            {
                Console.CursorTop = i;
                Console.Write(new string(' ', Console.WindowWidth - 1));
                Console.Write("\r");
            }

            int nameColumnSize = heroes.Max(h => h.Name.Length);
            //this is called ternary operator its basically an inline if. It reads "if name length > name column size then return name length, else return name column size"
            nameColumnSize = NameTitle.Length > nameColumnSize ? NameTitle.Length : nameColumnSize;
            nameColumnSize += 2; //we want to have some space

            int attackDamageColumnSize = heroes.Max(h => h.AttackDamage.ToString().Length);
            attackDamageColumnSize = AttackDamageTitle.Length > attackDamageColumnSize ? AttackDamageTitle.Length : attackDamageColumnSize;
            attackDamageColumnSize += 2;

            int spellPowerColumnSize = heroes.Max(h => h.SpellPower.ToString().Length);
            spellPowerColumnSize = SpellPowerTitle.Length > spellPowerColumnSize ? SpellPowerTitle.Length : spellPowerColumnSize;
            spellPowerColumnSize += 2;

            int healthColumnSize = heroes.Max(h => h.Health.ToString().Length);
            healthColumnSize = HealthTitle.Length > healthColumnSize ? HealthTitle.Length : healthColumnSize;
            healthColumnSize += 2;

            int armorColumnSize = heroes.Max(h => h.Armor.ToString().Length);
            armorColumnSize = ArmorTitle.Length > armorColumnSize ? ArmorTitle.Length : armorColumnSize;
            armorColumnSize += 2;

            int magicResistColumnSize = heroes.Max(h => h.MagicResist.ToString().Length);
            magicResistColumnSize = MagicResistTitle.Length > magicResistColumnSize ? MagicResistTitle.Length : magicResistColumnSize;
            magicResistColumnSize += 2;

            longestEnemyInfo = 
                1 + nameColumnSize + 
                1 + attackDamageColumnSize + 
                1 + spellPowerColumnSize + 
                1 + healthColumnSize + 
                1 + armorColumnSize + 
                1 + magicResistColumnSize + 1;

            int dashCount = (longestEnemyInfo - title.Length) / 2;
            string leftDashes = new string('-', dashCount);
            dashCount = longestEnemyInfo - title.Length - dashCount;
            string rightDashes = new string('-', dashCount);

            Console.WriteLine(leftDashes + title + rightDashes);

            Console.Write($"|{CenteredStringInCell(NameTitle, nameColumnSize)}|");
            Console.Write($"{CenteredStringInCell(AttackDamageTitle, attackDamageColumnSize)}|");
            Console.Write($"{CenteredStringInCell(SpellPowerTitle, spellPowerColumnSize)}|");
            Console.Write($"{CenteredStringInCell(HealthTitle, healthColumnSize)}|");
            Console.Write($"{CenteredStringInCell(ArmorTitle, armorColumnSize)}|");
            Console.Write($"{CenteredStringInCell(MagicResistTitle, magicResistColumnSize)}|{Environment.NewLine}");

            Console.WriteLine(leftDashes + new string('-', title.Length) + rightDashes);

            for (int i = 0; i < heroes.Length; i++)
            {
                Console.Write($"|{CenteredStringInCell(heroes[i].Name, nameColumnSize)}|");
                Console.Write($"{CenteredStringInCell(heroes[i].AttackDamage.ToString(), attackDamageColumnSize)}|");
                Console.Write($"{CenteredStringInCell(heroes[i].SpellPower.ToString(), spellPowerColumnSize)}|");
                Console.Write($"{CenteredStringInCell(heroes[i].Health.ToString(), healthColumnSize)}|");
                Console.Write($"{CenteredStringInCell(heroes[i].Armor.ToString(), armorColumnSize)}|");
                Console.Write($"{CenteredStringInCell(heroes[i].MagicResist.ToString(), magicResistColumnSize)}|{Environment.NewLine}");
            }

            Console.WriteLine(leftDashes + new string('-', title.Length) + rightDashes);
            
        }

        public static void ShowIntroMessage()
        {
            Console.WriteLine("Welcome to this awesome game, your goal is to kill all enemies");
            Console.WriteLine("To attack, press 'A'");
            Console.WriteLine("To use spell, press 'S'");
            Console.WriteLine("To quit, press 'Q'");
            Console.WriteLine("Good luck!");
            headerEndPosition = Console.CursorTop;
        }

        public static ConsoleKeyInfo ReadUserInput()
        {
            ConsoleKeyInfo input = Console.ReadKey(); //retrieves key input from user and prints it on screen
            Console.Write("\r"); //clears line, so users input disappears from screen
            return input;
        }

        public static string CreateSelectMenu(string title, List<string> options)
        {
            if(options.Count == 0)
            {
                return string.Empty;
            }

            int defaultCursorTopPosition = Console.CursorTop; //this is position where menu listing starts
            int currentCursorTopPosition = Console.CursorTop; //this will be position where menu listing ends
            int currentlySelected = 0;

            ConsoleKeyInfo key;
            do
            {
                //shows current state of menu
                Console.WriteLine(title);

                for (int i = 0; i < options.Count; i++)
                {
                    if (i == currentlySelected)
                    {
                        Console.Write(">> ");
                    }
                    Console.WriteLine($"{options[i]}");
                }

                //reads key, if down or up arrow is used, select menu changes state accordingly
                key = ReadUserInput();

                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (currentlySelected > 0)
                        {
                            currentlySelected--;
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        if (currentlySelected < options.Count - 1)
                        {
                            currentlySelected++;
                        }
                        break;
                }

                currentCursorTopPosition = Console.CursorTop;

                //refresh select menu
                for (int i = currentCursorTopPosition; i >= defaultCursorTopPosition; i--)
                {
                    Console.CursorTop = i;
                    Console.Write(new string(' ', Console.WindowWidth - 1));
                    Console.Write("\r");
                }
            } while (key.Key != ConsoleKey.Enter);

            return options[currentlySelected];
        }

        public static void StartTemporaryBlock()
        {
            ClearTemporaryLines();
            temporaryBlockStarted = true;
            temporaryLinePosition = Console.CursorTop;
        }

        public static void WriteToTemporaryBlock(string message)
        {
            if (temporaryBlockStarted)
            {
                Console.WriteLine(message);
            }
        }

        public static void EndTemporaryBlock()
        {
            temporaryBlockStarted = false;
        }

        public static void WriteTemporaryLine(string message)
        {
            if (temporaryBlockStarted)
            {
                WriteToTemporaryBlock(message);
            }

            ClearTemporaryLines();

            temporaryLinePosition = Console.CursorTop;
            Console.WriteLine(message);
        }

        public static string ReadToEnd()
        {
            //temporary comment
            string result = string.Empty;
            char nextCharacter;
            do
            {
                int nextCharacterNumber = Console.Read(); //we get ASCII representation of the character
                nextCharacter = (char)nextCharacterNumber; //we convert ASCII number to character
                if (!Environment.NewLine.Contains(nextCharacter))
                {
                    result += nextCharacter;
                }
            } while (!Environment.NewLine.EndsWith(nextCharacter.ToString())); //read characters while character is not the last part of the newline flag

            return result;
        }

        private static void ClearTemporaryLines()
        {
            if(temporaryLinePosition == -1)
            {
                return;
            }

            for (int i = Console.CursorTop; i >= temporaryLinePosition; i--)
            {
                Console.CursorTop = i;
                Console.Write(new string(' ', Console.WindowWidth - 1));
                Console.Write("\r");
            }

            temporaryLinePosition = -1;
        }

        private static string CenteredStringInCell(string value, int cellSize)
        {
            if(value.Length >= cellSize)
            {
                return value;
            }

            int spaceCount = (cellSize - value.Length) / 2;
            string rightSpaces = new string(' ', spaceCount);
            spaceCount = cellSize - value.Length - spaceCount;
            string leftSpaces = new string(' ', spaceCount);
            return $"{leftSpaces}{value}{rightSpaces}";
        }
    }
}
